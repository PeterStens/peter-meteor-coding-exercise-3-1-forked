/*****************************************************************************/
/* Npcharacter: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.npcharacter.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.npcharacter.helpers({
  items: function () {
    return NPCharacter.find({});
  }
});

/*****************************************************************************/
/* Npcharacter: Lifecycle Hooks */
/*****************************************************************************/
Template.npcharacter.created = function () {
};

Template.npcharacter.rendered = function () {
};

Template.npcharacter.destroyed = function () {
};