/*****************************************************************************/
/* Scenarios: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Scenarios.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Scenarios.helpers({
  scenarios: function () {
    return Scenario.find({});
    }
});

/*****************************************************************************/
/* Scenarios: Lifecycle Hooks */
/*****************************************************************************/
Template.Scenarios.created = function () {
};

Template.Scenarios.rendered = function () {
};

Template.Scenarios.destroyed = function () {
};