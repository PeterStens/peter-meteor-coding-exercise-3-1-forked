/*****************************************************************************/
/* Npcharacterresponse: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Npcharacterresponse.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Npcharacterresponse.helpers({
  npcresponsefinder: function () {
    return NPCharacterResponse.find({});
  }
});

/*****************************************************************************/
/* Npcharacterresponse: Lifecycle Hooks */
/*****************************************************************************/
Template.Npcharacterresponse.created = function () {
};

Template.Npcharacterresponse.rendered = function () {
};

Template.Npcharacterresponse.destroyed = function () {
};